import React from "react";
import { AppRegistry, View } from "react-native";
import HexToRGB from "./src/app/HexToRGB/HexToRGBComponent";
const App = () => (
  <View>
    <HexToRGB />
  </View>
);

AppRegistry.registerComponent("colorWebNative", () => App);
